#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import errno

if sys.platform.startswith("win"):
    import winreg

class WinReg:

    ## @brief Connect to the registry, and return the connection handle.
    ## @note Handle exceptions and return None or the connection handle.
    @staticmethod
    def Connect(computer_name, key):
        conn = None
        if sys.platform.startswith("win"):
            try:
                conn = winreg.ConnectRegistry(computer_name, key)
            except OSError as e:
                conn = None
        return conn

    ## @brief Get the key handle for a key and sub key.
    ## @note Handle exceptions and return None or the key handle.
    @staticmethod
    def OpenKey(key, sub_key, access = None):
        key_conn = None
        try:
            key_conn = winreg.OpenKey(key, sub_key, access) if access \
                                        else winreg.OpenKey(key, sub_key)
        except OSError as e:
            key_conn = None
        return key_conn

    ### @brief Get a value for a key and value name from the Windows registry.
    ## @note If there is an error or either keys is not set, None is returned.
    @staticmethod
    def GetKey(computer_name = None,
               key = winreg.HKEY_LOCAL_MACHINE,
               key_name = "",
               value_name = "",
               key_access = None):

        value = None
        conn = WinReg.Connect(computer_name, key)
        if conn:
            key_conn = WinReg.OpenKey(conn, key_name, key_access)
            if key_conn:
                value = winreg.QueryValueEx(key_conn, value_name)
                winreg.CloseKey(key_conn)

        return value[0] if value else None

if __name__ == "__main__":

    if len(sys.argv) > 2:
        # Example: "Console" and "VirtualTerminalLevel"
        print("{}".format(WinReg.GetKey(None, winreg.HKEY_CURRENT_USER, sys.argv[1], sys.argv[2])))
